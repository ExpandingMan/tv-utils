<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# `tv-utils`
> A plugin for [Oh My Fish][omf-link].

[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

This is a set of utilities that are used for controlling my TV machine.  See the full dotfiles
at [`dotv`](https://gitlab.com/ExpandingMan/dotv).


## Install

```fish
$ omf install https://gitlab.com/ExpandingMan/tv-utils.git
```

