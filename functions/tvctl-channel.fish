
function _tvctl-channel_pid -a name
    for ch in $TV_CHANNELS_ACTIVE

    end
end

#WARN:
# the below has been a disaster, first because all this stuff is
# asynchronous and you have no visibility whatsoever into the state
# of each thread (e.g. has the window drawn yet?)

# only hope of doing this simply I think is launching stuff via i3
# and trying to let i3 manage windows by putting them into specific
# workspaces that you can switch to from here

function _tvctl-channel_open -a name
    # check if channel already running, if so switch to it
    for ch in $TV_CHANNELS_ACTIVE
        if test $name = (string split -f 1 '%%' $ch)
            set pid (string split -f 2 '%%' $ch)
            xdotool search --pid $pid windowfocus
            return
        end
    end

    # otherwise run the channel
    for ch in $TV_CHANNELS
        if test $name = (string split -f 1 '%%' $ch)
            set exe (string split -f 2 '%%' $ch)
            # must split on whitespace to get a valid expression that can be run
            set exe (string split ' ' $exe)
            $exe &
            disown (jobs -lp)
            set pid $last_pid
            sleep 6
            set wid (xdotool search --limit 1 --pid $pid)
            echo $wid
            xdotool windowfocus $wid
        end
    end
end

function tvctl-channel
    argparse -n 'tvctl-channel' 'h/help' -- $argv

    if set -q _flag_h
        echo "**TODO**!! help string"
        return
    end

    test (count $argv) -eq 1 && _tvctl-channel_open $argv
end
