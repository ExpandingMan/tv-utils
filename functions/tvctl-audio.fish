
function __tvctl-audio_help
    echo "tv audio control **TODO** create docs!"
end

function tvctl-audio
    argparse -i -n tvctl-audio 'h/help' 't/toggle-mute' 'i/increase=' 'd/decrease=' -- $argv

    # whatever, don't know a good way of doing this right now; hash is out of range
    set __TV_AUDIO_ID '111'

    if set -q _flag_h
        __tvctl-audio_help
        return
    end

    if set -q _flag_i
        pamixer --unmute -i $_flag_i
        set vol (pamixer --get-volume)
        dunstify -u low -h int:value:$vol -t 600 -r $__TV_AUDIO_ID "🔊↑ ($vol%)"
    end

    if set -q _flag_d
        pamixer --unmute -d $_flag_d
        set vol (pamixer --get-volume)
        dunstify -u low -h int:value:$vol -t 600 -r $__TV_AUDIO_ID "🔊↓ ($vol%)"
    end

    if set -q _flag_t
        pamixer -t
        if pamixer --get-mute
            dunstify -u low -t 600 -r $__TV_AUDIO_ID "🔇 (mute)"
        else
            set vol (pamixer --get-volume)
            dunstify -u low -h int:value:$vol -t 600 -r $__TV_AUDIO_ID "🔊 (unmute: $vol%)"
        end
    end
end
