
function tvctl -a com
    if test $com = 'audio'
        tvctl-audio $argv
        return
    end

    # we do this after to give subcommands a chance to do it
    argparse -i -n 'tvctl' 'h/help' -- $argv

    if set -q _flag_h
        echo "help! **PLEASE WRITE THIS!**"
    end
end
