# tv-utils initialization hook
#
# You can use the following variables in this file:
# * $package       package name
# * $path          package path
# * $dependencies  package dependencies

# X11 window class to use to identify channels to switch to
set -xg TV_CHANNEL_WM_CLASS "brave-browser" "freetube"

set -xg TV_CHANNELS "netflix%%brave --app=https://netflix.com" \
                    "sling%%brave --app=https://sling.com" \
                    "amazon%%https://www.amazon.com/gp/video/storefront" \
                    "freetube%%freetube"

